package kata.yatzy;

import static java.lang.Math.multiplyExact;
import static java.util.Arrays.asList;
import static kata.yatzy.YatzyCalculator.*;

import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.IntStream;

public class Yatzy {

    public static int chance(int d1, int d2, int d3, int d4, int d5)
    {
        return IntStream.of(d1, d2, d3, d4, d5).sum();    }

    public static int yatzy(int... dice)
    {
        return IntStream.of(dice).allMatch(die -> die == dice[0])? 50:0;
    }

    public static int ones(int d1, int d2, int d3, int d4, int d5) {
        return summingScore(getArray(d1,d2,d3,d4,d5), 1);
    }

    public static int twos(int d1, int d2, int d3, int d4, int d5) {
        return summingScore(getArray(d1,d2,d3,d4,d5), 2);
    }

    public static int threes(int d1, int d2, int d3, int d4, int d5) {
        return summingScore(getArray(d1,d2,d3,d4,d5), 3);
    }

    protected int[] dice;
    public Yatzy(int d1, int d2, int d3, int d4, int d5)
    {
        dice = getArray(d1,d2,d3,d4,d5);
    }

    public int fours()
    {
        return summingScore(dice, 4);
    }

    public int fives()
    {
        return summingScore(dice, 5);
    }

    public int sixes()
    {
        return summingScore(dice, 6);
    }

    public static int scorePair(int d1, int d2, int d3, int d4, int d5)
    {
      return summingPeer(getArray(d1,d2,d3,d4,d5), 1);
    }

    public static int twoPair(int d1, int d2, int d3, int d4, int d5)
    {
      return summingPeer(getArray(d1,d2,d3,d4,d5), 2);
    }

    public static int fourOfAKind(int d1, int d2, int d3, int d4, int d5)
    {
      return summingOccurence(asList(d1,d2,d3,d4,d5), 4);
    }

    public static int threeOfAKind(int d1, int d2, int d3, int d4, int d5)
    {
      return summingOccurence(asList(d1,d2,d3,d4,d5), 3);
    }

    public static int smallStraight(int d1, int d2, int d3, int d4, int d5)
    {
        return hasSameContent(SMALL_STRAIGHTS, asList(d1,d2,d3,d4,d5))? 15:0;
    }

    public static int largeStraight(int d1, int d2, int d3, int d4, int d5)
    {
        return hasSameContent(LARGE_STRAIGHTS, asList(d1,d2,d3,d4,d5))? 20:0;
    }

    public static int fullHouse(int d1, int d2, int d3, int d4, int d5)
    {
        Set<Entry<Integer, Integer>> entries = groupingScore(getArray(d1,d2,d3,d4,d5)).entrySet();
        long nbrElement = entries.stream().filter(entry -> entry.getValue()>=2).count();
        if (nbrElement==2){
            return entries.stream()
                .mapToInt(entry -> multiplyExact(entry.getKey(), entry.getValue()))
                .sum();
        }
        return 0;
    }
}
