package kata.yatzy;

import static java.util.Arrays.asList;
import static java.util.Collections.frequency;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingInt;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.IntStream;

class YatzyCalculator {

    final static List SMALL_STRAIGHTS = asList(1, 2, 3, 4, 5);
    final static List LARGE_STRAIGHTS = asList(2, 3, 4, 5, 6);

    private YatzyCalculator() {
        // singleton
    }

    static int summingScore(int[] indices, int score) {
        return Math.toIntExact(IntStream.of(indices)
                .filter(die -> die == score)
                .count() * score);
    }


    static int summingPeer(int[] dice, int nbrOfPeer) {
        return groupingScore(dice).entrySet().stream()
            .filter(entry -> entry.getValue() >= 2)
            .mapToInt(entry -> entry.getKey()* 2)
            .limit(nbrOfPeer)
            .sum();
    }

    static LinkedHashMap<Integer, Integer> groupingScore(int[] dice) {
        return IntStream.of(dice)
            .boxed()
            .sorted(Comparator.reverseOrder())
            .collect(groupingBy(die -> die, LinkedHashMap::new, summingInt(die -> 1)));
    }

    static int summingOccurence(List<Integer> dice, int occurence) {
        return dice.stream()
            .filter(current -> frequency(dice, current) >= occurence)
            .findFirst().orElse(0) * occurence;
    }

    static boolean hasSameContent(List smallStraights, List dice) {
        return smallStraights.stream().allMatch(dice::contains);
    }
    static int[] getArray(int ...dice) {
        return dice;
    }
}
